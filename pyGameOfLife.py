from gameOfLife_helpers import updateBoard, makeRectangleList, getRandomBoard

import pygame
pygame.init()

pixel = 5
fieldDim = (300,150)
size = (pixel*fieldDim[0], pixel*fieldDim[1])

randomFraction = 0.3125

pygame.display.set_caption("PyGameOfLife")
screen = pygame.display.set_mode(size)

board = getRandomBoard(fieldDim[0],fieldDim[1],randomFraction)

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

gamePaused = False

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get(): # User did something
        if event.type == pygame.QUIT: # If user clicked close
            done = True # Flag that we are done so we exit this loop
            print("User asked to quit.")
        elif event.type == pygame.KEYDOWN:
            if gamePaused:
                gamePaused = False
                print("Resuming game")
            else:
                gamePaused = True
                print("Pausing game")
        elif event.type == pygame.MOUSEBUTTONDOWN:
            print("Creating new board")
            board = getRandomBoard(fieldDim[0],fieldDim[1],randomFraction)

    # --- Game logic should go here
    if not gamePaused:
        board = updateBoard(board)

    # --- Drawing code should go here

    # First, clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.
    screen.fill((0xFF,0xFF,0xFF))
    for color,rect in makeRectangleList(board,pixel,pixel):
        pygame.draw.rect(screen, color, rect)

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit frames per second
    clock.tick(2)

pygame.quit()
