def getRandomBoard(xDim,yDim,p):
    from random import random
    return [ [ 0 if random() > p else 1 for j in range(yDim) ] for i in range(xDim) ]

def nAliveNeighbours(board, x, y):
    neighbours = ((x-1,y-1), (x  ,y-1), (x+1,y-1),
                  (x-1,y  ),            (x+1,y  ), 
                  (x-1,y+1), (x  ,y+1), (x+1,y+1))
    xDim = len(board)
    yDim = len(board[0])
    nAliveNeighbours = 0
    for i,j in neighbours:
        if i < 0 or i >= xDim:
            continue
        if j < 0 or j >= yDim:
            continue
        if board[i][j] > 0:
            nAliveNeighbours += 1
    return nAliveNeighbours

def updateBoard(board,stayAlive=[2,3],getBorn=[3]):
    from copy import deepcopy
    nextStep = deepcopy(board)
    xDim = len(board)
    yDim = len(board[0])
    for i in range(xDim):
        for j in range(yDim):
            oldState = board[i][j]
            nNeighbours = nAliveNeighbours(board, i, j)
            if oldState <= 0:
                if nNeighbours in getBorn:
                    nextStep[i][j] = 1
                else:
                    nextStep[i][j] -= 1
            elif oldState > 0:
                if nNeighbours in stayAlive:
                    nextStep[i][j] += 1
                else:
                    nextStep[i][j] = -1
    return nextStep

def makeRectangleList(board, xLen, yLen):
    xDim = len(board)
    yDim = len(board[0])
    rectangleList = []
    for i in range(xDim):
        for j in range(yDim):
            rect = [i*xLen, j*yLen, xLen, yLen] 
            color = colorForState(board[i][j])
            if color is None:
                # No need to print empty squares
                continue
            rectangleList.append((color,rect))
    return rectangleList

def colorForState(state):
    BLACK = ( 0x00, 0x00, 0x00)
    WHITE = ( 0xFF, 0xFF, 0xFF)
    RED   = ( 0xFF, 0x00, 0x00)
    PINK  = ( 0xFF, 0x99, 0x99)
    GREEN = ( 0x11, 0xEE, 0x11)
    BLUE  = ( 0x00, 0x00, 0xFF)
    if state >= 2:
        return BLACK
    elif state >= 1:
        return GREEN
    elif state == 0:
        return WHITE
    elif state == -1:
        return PINK
    else:
        return None
